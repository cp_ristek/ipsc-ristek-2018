import java.util.List;
import java.util.ArrayList;

public class AntreHelper {

    static class SolutionEngine{
        private static List<Integer> solutionsX = new ArrayList<>();
        private static List<Integer> solutionsY = new ArrayList<>();

        static void answer(int x, int y) {
            solutionsX.add(x);
            solutionsY.add(y);
        }

        static void print_solution() {
            long M = 1000000007;
            long res = 0;
            long p = 1;
            for (int i = 0 ; i < solutionsX.size() ; i++) {
                p = 19L * p % M;
                
                int x = solutionsX.get(i);
                int y = solutionsY.get(i);

                long val = (((x * p) % M) * y) % M;
                res = (res + val) % M;
            }
            System.out.println(res);
        }
    }


    // Untuk memberikan jawaban, cukup panggil method answer(x, y) yang mana x adalah asal universitas mahasiswa dan y adalah nomor mahasiswanya setiap kali perintah tipe 2 dijalankan.
    // Pastikan method print_solution() dipanggil di bagian paling akhir dari main.
    // Anda cukup menyalin hasil keluaran dari method print_solution() kedalam template solusi yang diberikan.
    
    public static void main(String[] args) {
        // Lakukan proses input dan komputasi disini
        SolutionEngine.print_solution();
    }
}
