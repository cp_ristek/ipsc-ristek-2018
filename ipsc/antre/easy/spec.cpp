#include "tcframe/runner.hpp"
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

class ProblemSpec : public BaseProblemSpec {
   protected:
    const int MAX_N = 1e3;
    const int MAX_Q = 1e3;
    const int MAX_Y = 1e3;

    string label;
    int N, Q;
    vector<int> COMMANDS;
    vector<vector<int>> L;
    int ANS;

    void InputFormat() {
        LINE(label);
        LINE(N, Q);
        LINES(COMMANDS, L) % SIZE(Q);
    }

    void OutputFormat() { LINE(ANS); }

    void Constraints() {
        CONS(1 <= N && N <= MAX_N);
        CONS(1 <= Q && Q <= MAX_Q);
        CONS(L.size() == Q);
        CONS(validQuery());
    }

   private:
    bool validQuery() {
        set<int> s;
        int sz = 0, cnt = 0;
        for (int i = 0; i < Q; i++) {
            if (COMMANDS[i] == 1) {
                if (L[i].size() != 2) return false;
                if (1 > L[i][0] || N < L[i][0]) return false;
                if (1 > L[i][1] || MAX_Y < L[i][1]) return false;
                if (s.count(L[i][1])) return false;
                
                s.insert(L[i][1]);
                cnt++;
                sz++;

            } else if (COMMANDS[i] == 2) {
                if (L[i].size() != 0) return false;
                if (--sz < 0) return false;
            } else {
                return false;
            }
        }

        if (cnt != s.size()) return false;
        return true;
    }
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
   protected:
    void SampleTestCase1() {
        Input({
            "#0",
            "5 8",
            "1 3 2",
            "1 5 3",
            "1 3 1",
            "2",
            "2",
            "1 4 8",
            "2",
            "2"
        });

        Output({"4274354"});
    }

    void TestGroup1() {
        CASE(create_label(1), N = 10, Q = 20, randomQuery(N, Q, 1, 1, 10));
        CASE(create_label(2), N = 100, Q = 200, randomQuery(N, Q, 2, 1, 10));
        CASE(create_label(3), N = 998, Q = 1000, randomQuery(N, Q, 3, 1, 10));
        CASE(create_label(4), N = 999, Q = 1000, randomQuery(N, Q, 3, 1, 100));
        CASE(create_label(5), N = 1000, Q = 1000, randomQuery(N, Q, 3, 1, 1000));
    }

    void BeforeTestCase() {
        COMMANDS.clear();
        L.clear();
    }

    void AfterTestCase() {
        renumber();
    }

   private:
    void randomQuery(int n, int q, int empty, int min_x, int max_x) {
        vector<int> empty_events = sortedRandomArray(q / 2, min_x, max_x);
        transform(empty_events.begin(), empty_events.end(),
            empty_events.begin(), [](int i) { return i * 2; });
        empty_events.push_back(1e9);

        int cnt = 0, index_empty = 0;
        for (int i = 1; i <= q; i++) {
            while (i > empty_events[index_empty]) index_empty++;

            if (i + cnt == empty_events[index_empty]) {
                COMMANDS.push_back(2);
                L.push_back({});

            } else if (cnt == 0) {
                COMMANDS.push_back(1);
                L.push_back({ rnd.nextInt(1, n), i });

            } else {
                int command = rnd.nextInt(1, 2);
                if (command == 1) {
                    COMMANDS.push_back(1);
                    L.push_back({rnd.nextInt(1, n), i });
                } else {
                    COMMANDS.push_back(2);
                    L.push_back({});
                }
            }
        }
    }

    vector<int> sortedRandomArray(int n, int min_a, int max_a) {
        vector<int> v = randomArray(n, min_a, max_a);
        sort(v.begin(), v.end());
        return v;
    }

    vector<int> randomArray(int n, int min_a, int max_a) {
        vector<int> v;
        for (int i = 0; i < n; i++) {
            v.push_back(rnd.nextInt(min_a, max_a));
        }
        return v;
    }

    void renumber() {
        vector<int> tmp(N), tmp2(Q);
        // iota(tmp.begin(), tmp.end(), 1);
        iota(tmp2.begin(), tmp2.end(), 1);

        // for (int i = 0; i < (int) L.size(); i++) {
        //     if (COMMANDS[i] == 1)
        //         L[i][0] = tmp[L[i][0] - 1];
        // }

        for (int i = 0; i < (int) L.size(); i++) {
            if (COMMANDS[i] == 1)
                L[i][1] = tmp2[L[i][1] - 1];
        }
    }

    void create_label(int i) {
        ostringstream ss;
        ss << i;
        label = "#" + ss.str();
    }
};
