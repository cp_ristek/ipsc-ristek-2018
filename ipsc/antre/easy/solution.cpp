#include <bits/stdc++.h>
using namespace std;

struct {
    vector<int> solutions_x;
    vector<int> solutions_y;

    void answer(int x, int y) {
        solutions_x.push_back(x);
        solutions_y.push_back(y);
    }

    void print_solution() {
        long long M = 1000000007;
        long long res = 0;
        long long p = 1;

        for (int i = 0; i < solutions_x.size(); i++) {
            p = 19 * p % M;

            int x = solutions_x[i];
            int y = solutions_y[i];

            long long val = (((x * p) % M) * y) % M;
            res = (res + val) % M;
        }

        cout << res << '\n';
    }

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x, y) yang mana x
// adalah asal sekolah siswa dan y adalah nomor siswanya setiap kali perintah
// tipe 2 dijalankan. Pastikan prosedur print_solution() dipanggil di bagian
// paling akhir dari main. Anda cukup menyalin hasil keluaran dari prosedur
// print_solution() kedalam template solusi yang diberikan.

int n, q;
vector<pair<int, int>> main_queue;

void enqueue(int x, int y) {
    vector<pair<int, int>>::iterator position = main_queue.end();
    for (auto it = main_queue.begin(); it != main_queue.end(); it++) {
        if (it->first == x) {
            position = next(it);
        }
    }
    main_queue.insert(position, {x, y});
}

pair<int, int> pop() {
    pair<int, int> front = main_queue[0];
    main_queue.erase(main_queue.begin());
    return front;
}

int main() {
    char label[105];
    scanf("%s", label);
    scanf("%d %d", &n, &q);
    while (q--) {
        int command;
        scanf("%d", &command);

        if (command == 1) {
            int x, y;
            scanf("%d %d", &x, &y);
            enqueue(x, y);
        } else if (command == 2) {
            pair<int, int> front = pop();
            solutionEngine.answer(front.first, front.second);
        }
    }

    solutionEngine.print_solution();
    return 0;
}
