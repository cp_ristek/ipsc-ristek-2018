class SolutionEngine():

    solutions_x = []
    solutions_y = []

    def answer(self, x, y):
        self.solutions_x.append(x)
        self.solutions_y.append(y)

    def print_solution(self):
        M = 1000000007
        res = 0
        p = 1
        for i in range(len(self.solutions_x)):
            p = (p * 19) % M
            x = self.solutions_x[i]
            y = self.solutions_y[i]
            val = (x * p * y) % M
            res = (res + val) % M
        print(res)
solutionEngine = SolutionEngine()
# Untuk memberikan jawaban, cukup panggil method solutionEngine.answer(x, y) yang mana x adalah asal universitas mahasiswa dan y adalah nomor mahasiswanya setiap kali perintah tipe 2 dijalankan.
# Pastikan method solutionEngine.print_solution() dipanggil di bagian paling akhir dari program ini.
# Anda cukup menyalin hasil keluaran dari method solutionEngine.print_solution() kedalam template solusi yang diberikan.
    

# Lakukan proses input dan komputasi disini
solutionEngine.print_solution()