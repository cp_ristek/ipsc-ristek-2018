#include <bits/stdc++.h>
using namespace std;

struct {
    vector<int> solutions_x;
    vector<int> solutions_y;

    void answer(int x, int y) {
        solutions_x.push_back(x);
        solutions_y.push_back(y);
    }

    void print_solution() {
        long long M = 1000000007;
        long long res = 0;
        long long p = 1;

        for (int i = 0; i < solutions_x.size(); i++) {
            p = 19 * p % M;

            int x = solutions_x[i];
            int y = solutions_y[i];

            long long val = (((x * p) % M) * y) % M;
            cerr << val << endl;
            res = (res + val) % M;
        }

        cout << res << '\n';
    }

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x, y) yang mana x
// adalah asal sekolah siswa dan y adalah nomor siswanya setiap kali perintah
// tipe 2 dijalankan. Pastikan prosedur print_solution() dipanggil di bagian
// paling akhir dari main. Anda cukup menyalin hasil keluaran dari prosedur
// print_solution() kedalam template solusi yang diberikan.

const int N = 1e5;

int n, q;
queue<int> main_queue, school_queues[N + 5];

void enqueue(int x, int y) {
    if (school_queues[x].empty()) {
        main_queue.push(x);
    }
    school_queues[x].push(y);
}

pair<int, int> pop() {
    int x = main_queue.front();
    while (school_queues[x].empty()) {
        main_queue.pop();
        x = main_queue.front();
    }
    int y = school_queues[x].front();
    school_queues[x].pop();

    return {x, y};
}

int main() {
    char label[105];
    scanf("%s", label);
    scanf("%d %d", &n, &q);
    while (q--) {
        int command;
        scanf("%d", &command);

        if (command == 1) {
            int x, y;
            scanf("%d %d", &x, &y);
            enqueue(x, y);
        } else if (command == 2) {
            pair<int, int> front = pop();
            solutionEngine.answer(front.first, front.second);
        }
    }

    solutionEngine.print_solution();
    return 0;
}
