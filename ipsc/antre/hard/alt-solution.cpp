#include <bits/stdc++.h>
using namespace std;


struct{
	vector<int> solutions_x;
  vector<int> solutions_y;

	void answer(int x, int y){
		solutions_x.push_back(x);
    solutions_y.push_back(y);
	}

	void print_solution() {
		long long M = 1000000007;
		long long res = 0;
		long long p = 1;

		for (int i = 0 ; i < solutions_x.size() ; i++) {
			p = 19 * p % M;
      int x = solutions_x[i];
      int y = solutions_y[i];

			long long val = (((x * p) % M) * y) % M; 
			res = (res + val) % M;
		}

		cout << res << '\n';
	}

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x, y) yang mana x adalah asal sekolah siswa dan y adalah nomor siswanya setiap kali perintah tipe 2 dijalankan.
// Pastikan prosedur print_solution() dipanggil di bagian paling akhir dari main.
// Anda cukup menyalin hasil keluaran dari prosedur print_solution() kedalam template solusi yang diberikan.

queue<int> masterQueue;
queue<int> school[100005];

int main() {
	// Lakukan proses input dan komputasi disini
  string label; cin >> label;
  int n, q; cin >> n >> q;

  for (int i = 0 ; i < q ; i++) {
    int opt; scanf("%d", &opt);

    if (opt == 2) {
      int x = masterQueue.front();
      int y = school[x].front();

      solutionEngine.answer(x, y);

      school[x].pop();
      if (school[x].empty()) {
        masterQueue.pop();
      }
    } else {
      int x, y;
      scanf("%d %d", &x, &y);

      if (school[x].empty()) {
        masterQueue.push(x);
      }

      school[x].push(y);
    }
  }

	solutionEngine.print_solution();
	return 0;
}
