Cashback 90%

[Deskripsi Soal]

Sebuah perusahaan fintech sedang melakukan promosi besar-besaran dengan memberikan cashback sebesar 90% apabila membeli makanan di restoran Salt Dinner menggunakan HomePei. Ini membuat restoran Salt Dinner yang lokasinya memang dekat dengan N buah perguruan tinggi segera dibanjiri mahasiswa. Karena pembuatan pesanan membutuhkan waktu yang tidak sebentar, mahasiswa-mahasiswa tersebut membentuk antrean. Terdapat 2 jenis peristiwa dalam antrean tersebut, yaitu:

1. Mahasiswa dengan nomor y yang berasal dari universitas x datang.
    a. Jika di dalam antrean terdapat teman mahasiswa yang berasal dari universitas yang sama, ia akan menyela antrean dan mengantre tepat di belakang teman satu universitasnya yang paling belakang.
    b. Jika tidak ada, ia akan mengantre dari paling belakang.
2. Pesanan untuk mahasiswa yang paling depan selesai dibuat. Mahasiswa tersebut keluar dari antrean.

Tugas Anda adalah membuat sebuah program yang dapat menyimulasikan antrean tersebut.

[Format Masukan]

Baris pertama berisi label kasus uji.
Label kasus uji adalah suatu string "#S" yang mana S adalah nomor kasus uji tersebut. Kasus uji contoh adalah kasus uji nomor 0.

Baris kedua berisi dua buah bilangan bulat N dan Q, yang menyatakan banyak universitas dan banyak operasi antrean.

Q baris berikutnya berisi salah satu dari format berikut.

1 x y, yang artinya mahasiswa dengan nomor y yang berasal dari universitas x datang.
2, yang artinya mahasiswa yang paling depan keluar dari antrean.

[Format Keluaran]

Keluarkan sebuah bilangan bulat, yang menyatakan jumlahan dari (x_i * y_i * 19^i) modulo 10^9 + 7, yang mana i adalah urutan mahasiswa keluar dari antrean, x_i adalah asal universitas mahasiswa yang keluar pada urutan ke-i, dan y_i adalah nomor mahasiswa yang keluar pada urutan ke-i.

Apabila Anda kesulitan dalam menghitung nilai tersebut, Anda dapat menggunakan kode sumber pembantu yang telah kami sediakan di bagian paling bawah soal.

[Contoh Masukan 1 Easy]

#0
5 8
1 3 2
1 5 3
1 3 1
2
2
1 4 8
2
2

[Contoh Keluaran 1 Easy]

4274354

[Penjelasan Contoh]

Penjelasan untuk contoh kasus pertama:

i x y  x * y * 19^i
1 3 2       114
2 3 1      1083
3 5 3    102885
4 4 8   4170272
------------------ +
        4274354

4274354 mod (10^9 + 7) = 4274354, sehingga hasilnya 4274354.

[Batasan]

- Dijamin tidak ada dua mahasiswa dengan nilai y yang sama
- Dijamin antrean tidak kosong saat peristiwa tipe 2 terjadi

[Batasan Easy]

- 1 ≤ N, Q ≤ 1.000
- 1 ≤ x ≤ N
- 1 ≤ y ≤ 1.000

[Batasan Hard]

- 1 ≤ N, Q ≤ 100.000
- 1 ≤ x ≤ N
- 1 ≤ y ≤ 100.000
