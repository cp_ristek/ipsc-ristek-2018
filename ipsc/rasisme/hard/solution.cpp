#include <bits/stdc++.h>
using namespace std;

void solve() {
    int n;
    cin >> n;

    string s;
    cin >> s;

    char same;

    if (s[0] != s[n-1]) same = s[1];
    else same = s[0];

    int ans = -1;
    for (int i = 0; i < n; i++) {
        if (s[i] != same) {
            ans = i;
            break;
        }
    }

    cout << ans << endl;
}

int main() {
    string label;
    cin >> label;
    solve();
}