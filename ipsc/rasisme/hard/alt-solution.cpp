#include <bits/stdc++.h>
using namespace std;

int main() {
  int n;
  string label, s;
  cin >> label >> n >> s;

  if (s[0] != s[1] && s[0] != s[2]) {
    cout << 0 << endl;
  } else {
    for (int i = 1 ; i < n ; i++)
      if (s[i] != s[0]) {
        cout << i << endl;
        break;
      }
  }
  
  return 0;
}