#include <bits/stdc++.h>
using namespace std;


int main() {
  string label; cin >> label;
  int n; cin >> n;
  string s; cin >> s;

  for (int i = 0 ; i < n ; i++) {
    bool ok = true;
    for (int j = 0 ; j < n ; j++) {
      if (i != j && s[i] == s[j]) {
        ok = false;
      }
    }
    if (ok) {
      printf("%d\n", i);
      break;
    }
  }
  return 0;
}