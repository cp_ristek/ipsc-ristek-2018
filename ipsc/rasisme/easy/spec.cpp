#include <tcframe/spec.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

const int MAX_N = 10;

class ProblemSpec : public BaseProblemSpec {
protected:
    string label;

    int N;
    string S;

    int ans;

    void InputFormat() {
        LINE(label);
        LINE(N);
        LINE(S);
    }

    void OutputFormat() {
        LINE(ans);
    }

    void GradingConfig() {
        TimeLimit(1);
        MemoryLimit(64);
    }

    void Constraints() {
        CONS(N == S.length());
        CONS(inBetween(S.length(), 3, MAX_N));
        CONS(isValid(S));
    }

private:
    bool inBetween(int x, int lo, int hi) {
        return lo <= x && x <= hi;
    }

    bool isValid(string s) {
        char same;
        int size = s.length();
        if (s[0] != s[size-1]) same = s[1];
        else s = s[0];

        int diff_cnt = 0;
        for (char c : s) {
            if (c != same) diff_cnt++;
        }
        return diff_cnt == 1;
    }
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
    void SampleTestCase1() {
        Input({
            "#0",
            "3",
            "AAB"
        });
        Output({
            "2"
        });
    }

    int currLabel = 1;
    void AfterTestCase() {
        label = createLabel(currLabel++);
    }

    void TestGroup1() {
        CASE(N = MAX_N, S = createValidString(N, 'Q', 'O'));
        CASE(N = MAX_N, S = createValidString(N, 'E', 'F'));
        CASE(N = MAX_N, S = createValidString(N, '8', 'B'));
        CASE(N = MAX_N, S = createValidString(N, 'o', 'e', 0));
        CASE(N = MAX_N, S = createValidString(N, 'v', 'w', N-1));
    }

private:
    string createLabel(int i) {
        ostringstream ss;
        ss << i;
        return "#" + ss.str();
    }

    char getRandomChar() {
        int x = rnd.nextInt(62);
        if (x < 10) {
            return '0' + x;
        } else if (x < 36) {
            return 'A' + x - 10;
        } else {
            return 'a' + x - 36;
        }
    }

    string createValidString(int size, char same = 0, char diff = 0, int pos = -1) {
        string ret = "";

        if (same == 0) {
            same = getRandomChar();
        }

        if (diff == 0) {
            do {
                diff = getRandomChar();
            } while (diff == same);
        }

        if (pos == -1) {
            pos = rnd.nextInt(size);
        }

        for (int i = 0; i < size; i++) {
            if (i == pos) ret+= diff;
            else ret+= same;
        }

        return ret;
    }

};