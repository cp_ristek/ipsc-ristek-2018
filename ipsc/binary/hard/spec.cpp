#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N,ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 100000);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"90"});
		Output({"111111103"});
	}

	void TestGroup1() {
		for (int i=1;i<=8;++i) {
			CASE(create_label(i), N = rnd.nextInt(50000, 99998));
		}
        CASE(create_label(9), N = 99999);
        CASE(create_label(10), N = 100000);
	}

private:

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};