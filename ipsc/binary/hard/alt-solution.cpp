#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;
const int MOD = 1e9 + 7;

int dist[N];
int n;
string label;
int main() {
  memset(dist, -1, sizeof dist);
  cin >> label >> n;

  dist[1 % n] = 1;
  queue<int> q; q.push(1 % n);

  while (!q.empty()) {
    int now = q.front();
    q.pop();

    for (int i = 0 ; i < 2 ; i++) {
      int nex = (10 * now + i) % n;
      int ndis = (10ll * dist[now] + i) % MOD;

      if (dist[nex] == -1) {
        dist[nex] = ndis;
        q.push(nex);
      }
    }
  }

  cout << dist[0] << endl;
  return 0;
}