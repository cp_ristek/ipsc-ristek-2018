#include <bits/stdc++.h>
using namespace std;

typedef pair<int,long long> pii;
const int MOD = 1e9+7;

int N;
string label;
bool vis[100005];
queue<pii> bfq;

int main() {
    cin >> label;
    cin >> N;

    bfq.push({1%N, 1%MOD});
    vis[1%N] = true;
    while (!bfq.empty()) {
        pii u = bfq.front(); bfq.pop();
        if (u.first == 0) {
            cout << u.second << '\n';
            return 0;
        }

        
        if (!vis[(u.first*10) % N]) {
            vis[(u.first*10) % N] = true;
            bfq.push({(u.first*10) % N, (u.second*10) % MOD});
        }

        if (!vis[(u.first*10 + 1) % N]) {
            vis[(u.first*10 + 1) % N] = true;
            bfq.push({(u.first*10 + 1) % N, (u.second*10 + 1) % MOD});
        }
    }

    assert(false);

    return 0;
}