#include <bits/stdc++.h>
using namespace std;

int N;
string label;

bool all_zero_one(int x) {
    while (x) {
        if (x%10 > 1)
            return false;
        x/=10;    
    }
    return true;
}

int main() {
    cin >> label;
    cin >> N;
    int x = 0;
    do {
        do {
            ++x;
        } while (!all_zero_one(x));
    } while (x%N != 0);

    cout << x << '\n';

    return 0;
}