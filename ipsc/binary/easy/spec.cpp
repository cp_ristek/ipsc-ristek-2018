#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N,ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 10);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"6"});
		Output({"1110"});
	}

	void TestGroup1() {
		for (int i=1;i<=10;++i) {
			CASE(create_label(i), N = i);
		}
	}

private:

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};