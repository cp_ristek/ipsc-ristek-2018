#include <bits/stdc++.h>
using namespace std;

int main() {
  string label; cin >> label;
  int n; cin >> n;

  int result[] = {0, 1, 10, 111, 100, 10, 1110, 1001, 1000, 111111111, 10};

  printf("%d\n", result[n]);
  return 0;
}