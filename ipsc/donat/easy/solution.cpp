#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
typedef long long ll;

struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		int M = 1000000007;
		long long res = 0;
		long long p = 19;
		for (int sol : solutions) {
			res = (res + (p*sol)%M)%M;
			p = (p*19)%M;
		}
		cout << res << '\n';
	}

} solutionEngine;

set<ll> nums;
vector<ll> numbers;
ll q, l, r;
string label;

void generate(){
	nums.insert(0);
	for(int i=0; i<=60; i++){
		for(int j=i; j<=60; j++){
			for(int k=j; k<=60; k++){
				// 1 bit nyala
				if(j == i && k==i) nums.insert(1ll << i);

				// 2 bit nyala
				else if(i == j && i != k){
					ll x = (1ll << i) + (1ll << k);
					nums.insert(x);
				}
				else if(i == k && i != j){
					ll x = (1ll << i) + (1ll << j);
					nums.insert(x);
				}
				else if(j == k && i != j){
					ll x = (1ll << j) + (1ll << i);
					nums.insert(x);
				}

				// 3 bit nyala
				else{
					ll x = (1ll << i) + (1ll << j) + (1ll << k);
					nums.insert(x);
				}
			}
		}
	}
}

int main() {
	generate();
	for(auto val : nums){
		numbers.push_back(val);
	}
	cin >> label;
	cin >> q;
	ll x;
	for(int i=0; i<q; i++){
		cin >> l >> r;
		if(l == 0){
			x = (upper_bound(numbers.begin(), numbers.end(), r) - numbers.begin());
		} else{
			x = (upper_bound(numbers.begin(), numbers.end(), r) 
				- upper_bound(numbers.begin(), numbers.end(), l-1));
		}
		// cout << x << "\n";
		solutionEngine.answer(x);
	}

	// cout << (1ll << 50) << "\n";

	solutionEngine.print_solution();
	return 0;
}
