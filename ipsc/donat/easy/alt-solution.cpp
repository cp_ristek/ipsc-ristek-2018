#include <bits/stdc++.h>
using namespace std;


struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		long long M = 1000000007;
		long long res = 0;
		long long p = 1;

		for (int sol : solutions) {
			p = 19 * p % M;
			long long val = (sol * p) % M; 
			res = (res + val) % M;
		}

		cout << res << '\n';
	}

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x) yang mana x adalah jawaban pada query ke-i.
// Pastikan prosedur print_solution() dipanggil di bagian paling akhir dari main.
// Anda cukup menyalin hasil keluaran dari prosedur print_solution() kedalam template solusi yang diberikan.

int solve(int l, int r) {
  int ret = 0;
  for (int i = l ; i <= r ; i++) {
    ret += (__builtin_popcount(i) <= 3);
  }
  return ret;
}

int main() {
	// Lakukan proses input dan komputasi disini
	string label; cin >> label;
  int q; scanf("%d", &q);

  for (int i = 0 ; i < q ; i++) {
    int l, r; scanf("%d %d", &l, &r);

    solutionEngine.answer(solve(l, r));
  }

	solutionEngine.print_solution();
	return 0;
}
