#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int Q;
	vector<int> L, R;
	int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(Q);
		LINES(L, R) % SIZE(Q);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= Q && Q <= 1000);
		CONS(eachElementBetween(L, 1, 1000));
		CONS(eachElementBetween(R, 1, 1000));
		CONS(checkRange(L,R,Q));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}

	bool checkRange(vector<int> &l, vector<int> &r, int q){
		for(int i=0; i<q; i++){
			if(l[i] > r[i]) {
				return false;
			}
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"2",
				"3 7",
				"10 16"});
		Output({"2261"});
	}

	void BeforeTestCase() {
		L.clear();
		R.clear();
	}

	void TestGroup1() {
		for (int i=1;i<=2;++i) {
			CASE(create_label(i), Q = rnd.nextInt(20,300), generate(Q));
		}
		for (int i=3;i<=4;++i) {
			CASE(create_label(i), Q = rnd.nextInt(301,1000), generate(Q));
		}
		CASE(create_label(5), Q = 50, generate_tricky(Q));
	}

private:

	void generate(int q) {
		for(int i=0; i<q; i++){
			L.push_back(rnd.nextInt(1,850));
			R.push_back(rnd.nextInt(L[i], 1000));
		}
	}

	void generate2(int q, int pos) {
		for(int i=0; i<q; i++){
			L.push_back(rnd.nextInt(1,850));
			R.push_back(rnd.nextInt(L[i+pos], 1000));
		}
	}

	void generate_tricky(int q){
		generate2(25, 50-q);
		L.push_back(1);
		R.push_back(1000);
		q -= 26;

		generate2(3, 50-q);
		L.push_back(1);
		R.push_back(66);
		q -= 4;

		generate2(3, 50-q);
		L.push_back(524);
		R.push_back(1000);
		q -= 4;

		generate2(2, 50-q);
		L.push_back(2);
		R.push_back(999);
		q -= 3;

		generate2(2, 50-q);
		L.push_back(546);
		R.push_back(546);
		q -= 3;

		generate2(2, 50-q);
		L.push_back(610);
		R.push_back(610);
		q -= 3;

		generate2(2, 50-q);
		L.push_back(1);
		R.push_back(1);
		L.push_back(1000);
		R.push_back(1000);
		q -= 4;

		generate2(q, 50-q);
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};
