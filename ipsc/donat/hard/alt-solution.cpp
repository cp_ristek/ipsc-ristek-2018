#include <bits/stdc++.h>
using namespace std;


struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		long long M = 1000000007;
		long long res = 0;
		long long p = 1;

		for (int sol : solutions) {
			p = 19 * p % M;
			long long val = (sol * p) % M; 
			res = (res + val) % M;
		}

		cout << res << '\n';
	}

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x) yang mana x adalah jawaban pada query ke-i.
// Pastikan prosedur print_solution() dipanggil di bagian paling akhir dari main.
// Anda cukup menyalin hasil keluaran dari prosedur print_solution() kedalam template solusi yang diberikan.

const int MX = 62;

vector<long long> v;

int solve(long long l, long long r) {
  return upper_bound(v.begin(), v.end(), r) - lower_bound(v.begin(), v.end(), l);
}

int main() {
  for (int i = 0 ; i <= MX ; i++) {
    v.push_back(1ll << i);

    for (int j = i+1 ; j <= MX ; j++) {
      v.push_back((1ll << i) | (1ll << j));

      for (int k = j+1 ; k <= MX ; k++) {
        v.push_back((1ll << i) | (1ll << j) | (1ll << k));
      }
    }
  }
  sort(v.begin(), v.end());

	// Lakukan proses input dan komputasi disini
	string label; cin >> label;
  int q; scanf("%d", &q);

  for (int i = 0 ; i < q ; i++) {
    long long l, r; scanf("%lld %lld", &l, &r);

    solutionEngine.answer(solve(l, r));
  }
  
	solutionEngine.print_solution();
	return 0;
}
