#include <tcframe/spec.hpp>
using namespace tcframe;

const int BILLION = 1000000000;

const long long MX = 1ll * BILLION * BILLION;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int Q;
	vector<long long> L, R;
	long long ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(Q);
		LINES(L, R) % SIZE(Q);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= Q && Q <= 100000);
		CONS(eachElementBetween(L, 1, MX));
		CONS(eachElementBetween(R, 1, MX));
		CONS(checkRange(L,R,Q));
	}

private:
	bool eachElementBetween(vector<long long> &v, long long l, long long r) {
		for (long long num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}

	bool checkRange(vector<long long> &l, vector<long long> &r, int q){
		for(int i=0; i<q; i++){
			if(l[i] > r[i]) return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"10",
				"5 1000",
				"900 90000",
				"2000000 9000000",
				"1234567890 9876543210",
				"112233445566778899 998877665544332211",
				"2017 2018",
				"2017201720172017 2018201820182018",
				"111111111111111 999999999999999",
				"123454321 678909876",
				"1010101010 9090909090"});
		Output({"179477863"});
	}

	void BeforeTestCase() {
		L.clear();
		R.clear();
	}

	void TestGroup1() {
		for (int i=1;i<=2;++i) {
			CASE(create_label(i), Q = rnd.nextInt(10000,50000), generate(Q));
		}
		for (int i=3;i<=4;++i) {
			CASE(create_label(i), Q = rnd.nextInt(50000,100000), generate2(Q));
		}
		CASE(create_label(5), Q = 87678, generate3(Q));
	}

private:

	void generate(int q) {
		for(int i=0; i<q; i++){
			L.push_back(rnd.nextLongLong(1,MX));
			R.push_back(rnd.nextLongLong(L[i], MX));
		}
	}

	void generate2(int q) {
		for(int i=0; i<q; i++){
			L.push_back(rnd.nextLongLong(1,MX / 2));
			R.push_back(rnd.nextLongLong(L[i]+MX/2, MX));
		}
	}

	void generate3(int q) {
		generate4(50000, 87678-q);
		L.push_back(1);
		R.push_back(MX);
		q -= 50001;


		generate4(1000, 87678-q);
		L.push_back(66);
		R.push_back(18790481920ll);
		q -= 1001;

		generate4(5000, 87678-q);
		L.push_back(1142461300736ll);
		R.push_back(MX);
		q -= 5001;

		generate4(1000, 87678-q);
		L.push_back(2);
		R.push_back(999999999999999999ll);
		q -= 1001;

		generate4(1000, 87678-q);
		L.push_back(2199024304160ll);
		R.push_back(2199024304160ll);
		q -= 1001;

		generate4(1000, 87678-q);
		L.push_back(1100586419200ll);
		R.push_back(1100586419200ll);
		q -= 1001;

		generate4(1000, 87678-q);
		L.push_back(1);
		R.push_back(1);
		L.push_back(1000000000000000000ll);
		R.push_back(1000000000000000000ll);
		q -= 1002;

		generate4(q, 87678-q);
	}

	void generate4(int q, int pos) {
		for(int i=0; i<q; i++){
			L.push_back(rnd.nextLongLong(1,MX/2));
			R.push_back(rnd.nextLongLong(L[i+pos]+MX/2, MX));
		}
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};
