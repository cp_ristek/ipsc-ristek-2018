import java.util.List;
import java.util.ArrayList;

public class DonatHelper {

    static class SolutionEngine{
        private static List<Integer> solutions = new ArrayList<>();

        static void answer(int x) {
            solutions.add(x);
        }

        static void print_solution() {
            long M = 1000000007;
            long res = 0;
            long p = 1;
            for (int sol : solutions) {
                p = 19L * p % M;
                long val = (sol * p) % M;
                res = (res + val) % M;
            }
            System.out.println(res);
        }
    }


    // Untuk memberikan jawaban, cukup panggil method answer(x) yang mana x adalah jawaban pada pertanyaan ke-i.
    // Pastikan method print_solution() dipanggil di bagian paling akhir dari main.
    // Anda cukup menyalin hasil keluaran dari method print_solution() kedalam template solusi yang diberikan.
    
    public static void main(String[] args) {
        // Lakukan proses input dan komputasi disini
        SolutionEngine.print_solution();
    }
}
