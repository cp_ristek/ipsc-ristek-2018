class SolutionEngine():

    solutions = []

    def answer(self, x):
        self.solutions.append(x)

    def print_solution(self):
        M = 1000000007
        res = 0
        p = 1
        for sol in self.solutions:
            p = (p * 19) % M
            val = (sol * p) % M
            res = (res + val) % M
        print(res)
solutionEngine = SolutionEngine()
# Untuk memberikan jawaban, cukup panggil method solutionEngine.answer(x) yang mana x adalah jawaban pada pertanyaan ke-i.
# Pastikan method solutionEngine.print_solution() dipanggil di bagian paling akhir dari program ini.
# Anda cukup menyalin hasil keluaran dari method solutionEngine.print_solution() kedalam template solusi yang diberikan.
    

# Lakukan proses input dan komputasi disini
solutionEngine.print_solution()