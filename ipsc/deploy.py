from os import listdir, system, getenv

slugs = ["antre", "banana", "binary", "donat", "rasisme", "rata", "selisih"]
variants = ["easy", "hard"]

def deploy_template(slug):
    for variant in variants:
        directory = "{}/{}".format(slug, variant)
        zipped = "{}-{}-solution-template.zip".format(slug, variant)

        system("zip -j {} {}/*template* {}/*Template*".format(zipped, directory, directory))
        system("mv {} {}/deploy/".format(zipped, slug))

# hacky, but i am already too sleepy to find good solution
def deploy_tc(slug):
    for variant in variants:
        directory = "{}/{}".format(slug, variant)
        zipped = "{}-{}-tc.zip".format(slug, variant)
        zipped_all = "{}-{}-input-output.zip".format(slug, variant)

        system("cp {}/solution.cpp .".format(directory))
        system("cp {}/spec.cpp .".format(directory))
        system("g++ -std=c++11 solution.cpp -o solution")
        system("/bin/bash $TCFRAME_HOME/scripts/tcframe build")
        system("./runner")

        files = listdir("tc")
        for f in files:
            if ".in" in f:
                dotF = f.find(".")
                simpleName = f[:dotF]
                newFile = "{}-{}".format(slug, variant)
                
                if "sample" in f:
                    newFile = "{}_0".format(newFile)
                else:
                    dot = f.find(".")
                    under = f.find("_")
                    under = f.find("_", under+1)
                    tcNum = f[under+1:dot]
                    newFile = "{}_{}".format(newFile, tcNum)
                
                system("mv tc/{}.in tc/{}.in".format(simpleName, newFile))
                system("mv tc/{}.out tc/{}.out".format(simpleName, newFile))

        system("zip -j {} tc/*in".format(zipped))
        system("zip -j {} tc/*in tc/*out".format(zipped_all))
        system("mv {} {}/deploy".format(zipped, slug))
        system("mv {} {}/deploy".format(zipped_all, slug))
        system("rm -r runner solution tc solution.cpp spec.cpp")

def deploy_helper(slug):
    if slug not in ["antre", "banana", "donat"]:
        return

    directory = "{}".format(slug)
    zipped = "{}-solution-helper.zip".format(slug)

    system("zip -j {} {}/*helper* {}/*Helper*".format(zipped, directory, directory))
    system("mv {} {}/deploy/".format(zipped, slug))

for slug in slugs:
    system("mkdir {}/deploy".format(slug))
    deploy_template(slug)
    deploy_tc(slug)
    deploy_helper(slug)