#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
    string label;
    int N;
    vector<int> A;
    int ANS;

    void InputFormat() {
        LINE(label);
        LINE(N);
        LINE(A % SIZE(N));
    }

    void OutputFormat() {
        LINE(ANS);
    }

    void StyleConfig() {
        TimeLimit(1);
    }

    void Constraints() {
        CONS(1 <= N && N <= 100000);
        CONS(eachElementBetween(A, 1, 1000*1000*1000));
    }

private:
    bool eachElementBetween(vector<int> &v, int l, int r) {
        for (int num : v) {
            if (num < l || num > r)
                return false;
        }
        return true;
    }
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

    void SampleTestCase1() {
        Input({"#0",
            "4",
            "1 4 4 1"});
        Output({"4"});
    }

    void BeforeTestCase() {
        A.clear();
    }

    void TestGroup1() {
        int maximum = 1000 * 1000 * 1000;
        CASE(create_label(1), N = 99937, generate(N, maximum));
        CASE(create_label(2), N = 100000, generate(N, 1232, 933243232, maximum));
        CASE(create_label(3), N = 100000, generate(N, 347382948, 347382948, maximum));
        CASE(create_label(4), N = 99999, generate(N, maximum));
        CASE(create_label(5), N = 100000, generate(N, 5, maximum-4, maximum));
    }

private:

    void generate(int n, int mid1, int mid2, int maximum) {
        for (int i = 1; i <= n/2-1; i++)
            A.push_back(rnd.nextInt(1, mid1));
        A.push_back(mid1);
        A.push_back(mid2);
        for (int i = 1; i <= n/2-1; i++)
            A.push_back(rnd.nextInt(mid2, maximum));
        rnd.shuffle(A.begin(),A.end());
    }   

    void generate(int n, int maximum) {
        for (int i = 1; i <= n; i++)
            A.push_back(rnd.nextInt(1, maximum));
    }

    void create_label(int i) {
        ostringstream ss;
        ss << i;
        label = "#" + ss.str();
    }
};
