#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const int N = 100003;
string label;
ll n, ans, a[N];

int main() {
	cin >> label;
	cin >> n;
	for (ll i = 0; i < n; i++) cin >> a[i];
	sort(a, a+n);
	if (n%2) ans = 1;
	else ans = a[n/2]-a[n/2-1]+1;
	cout << ans << '\n';
	return 0;
}