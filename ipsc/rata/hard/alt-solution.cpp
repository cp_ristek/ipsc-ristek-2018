#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;

int n;
int arr[N];

void read() {
  string label; cin >> label;
  
  cin >> n;
  for (int i = 0 ; i < n ; i++)
    cin >> arr[i];
}

int work() {
  if (n % 2 == 1) return 1;

  sort(arr, arr + n);

  return arr[n / 2] - arr[n / 2 - 1] + 1;
}

int main() {
  read();
  cout << work() << endl;
  return 0;
}