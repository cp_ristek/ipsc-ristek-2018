#include <bits/stdc++.h>
using namespace std;

const int N = 1005;
const int MX = 1000;

int n;
int arr[N];

void read() {
  string label; cin >> label;
  
  cin >> n;
  for (int i = 0 ; i < n ; i++)
    cin >> arr[i];
}

int calc(int x) {
  int ret = 0;
  for (int i = 0 ; i < n ; i++)
    ret += abs(x - arr[i]);
  return ret;
}

int work() {
  int mins = 2e9;
  int ret = 0;

  for (int i = 0 ; i <= MX ; i++) {
    int val = calc(i);

    if (val < mins) {
      ret = 1;
      mins = val;
    } else if (val == mins) {
      ret++;
    }
  }

  return ret;
}

int main() {
  read();
  cout << work() << endl;
  return 0;
}