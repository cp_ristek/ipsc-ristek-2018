#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N;
	vector<int> A;
	int ANS;

	void InputFormat() {
		LINE(label);
		LINE(N);
		LINE(A % SIZE(N));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000);
		CONS(eachElementBetween(A, 1, 1000));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
			"4",
			"1 4 4 1"});
		Output({"4"});
	}

	void BeforeTestCase() {
		A.clear();
	}

	void TestGroup1() {
		CASE(create_label(1), N = 924, generate(N, 696, 696));
		CASE(create_label(2), N = 2, generate(N, 174, 174));
		CASE(create_label(3), N = 1000, generate(N, 5, 995));
		CASE(create_label(4), N = 839, generate(N));
		CASE(create_label(5), N = 1000, generate(N, 423, 857));
	}

private:

	void generate(int n, int mid1, int mid2) {
		for (int i = 1; i <= n/2-1; i++)
			A.push_back(rnd.nextInt(1, mid1));
		A.push_back(mid1);
		A.push_back(mid2);
		for (int i = 1; i <= n/2-1; i++)
			A.push_back(rnd.nextInt(mid2, n));
		rnd.shuffle(A.begin(),A.end());
	}

	void generate(int n) {
		for (int i = 1; i <= n; i++)
			A.push_back(rnd.nextInt(1, n));
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};
