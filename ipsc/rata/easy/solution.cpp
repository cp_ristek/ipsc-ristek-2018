#include <bits/stdc++.h>
using namespace std;

const int N = 1003;
string label;
int n, ans, a[N];

int main() {
	cin >> label;
	cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	sort(a, a+n);
	if (n%2) ans = 1;
	else ans = a[n/2]-a[n/2-1]+1;
	cout << ans << '\n';
	return 0;
}