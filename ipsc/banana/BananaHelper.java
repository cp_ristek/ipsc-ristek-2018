import java.util.List;
import java.util.ArrayList;

public class BananaHelper {

    static class SolutionEngine{
        private static List<Integer> solutions = new ArrayList<>();

        static void answer(int x) {
            solutions.add(x);
        }

        static void print_solution() {
            long M = 1000000007;
            long res = 0;
            long idx = 1;
            for (int sol : solutions) {
                long val = (((idx * sol) % M) * sol) % M;
                res = (res + val) % M;
                idx++;
            }
            System.out.println(res);
        }
    }

    // Fungsi ini akan mengembalikan sekuens berukuran n+1, dengen seq[0] = -1 dan seq[i] = P_i untuk i > 0
    static List<Integer> generateSequence(int n, int a, int b) {
        ArrayList<Integer> seq = new ArrayList<>();
        seq.add(-1);

        for (int i = 1 ; i <= n ; i++) {
            long val = (((1L * i * a) % n) + b) % n;
            seq.add((int)val + 1);
        }

        return seq;
    }

    // Untuk memberikan jawaban, cukup panggil method answer(x) yang mana x adalah monyet yang memakan pisang ke-i.
    // Pastikan method print_solution() dipanggil di bagian paling akhir dari main.
    // Anda cukup menyalin hasil keluaran dari method print_solution() kedalam template solusi yang diberikan.
    
    public static void main(String[] args) {
        // Lakukan proses input dan komputasi disini

        SolutionEngine.print_solution();
    }
}
