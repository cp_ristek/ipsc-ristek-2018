class SolutionEngine():

    solutions = []

    def answer(self, x):
        self.solutions.append(x)

    def print_solution(self):
        M = 1000000007
        res = 0
        idx = 1
        for sol in self.solutions:
            val = (idx * sol * sol) % M
            res = (res + val) % M
            idx += 1
        print(res)

# Fungsi ini akan mengembalikan sekuens berukuran n+1, dengen seq[0] = -1 dan seq[i] = P_i untuk i > 0
def generate_sequence(n, a, b):
    seq = [-1]
    for i in range(1, n+1):
        val = ((i * a) + b) % n
        seq.append(val + 1)
    return seq

solutionEngine = SolutionEngine()
# Untuk memberikan jawaban, cukup panggil method solutionEngine.answer(x) yang mana x adalah monyet yang memakan pisang ke-i.
# Pastikan method solutionEngine.print_solution() dipanggil di bagian paling akhir dari program ini.
# Anda cukup menyalin hasil keluaran dari method solutionEngine.print_solution() kedalam template solusi yang diberikan.
    

# Lakukan proses input dan komputasi disini
solutionEngine.print_solution()