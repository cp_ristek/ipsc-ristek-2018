#include <tcframe/runner.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N, A, B;
	int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(N, A, B);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000);
    CONS(1 <= A && A < N);
    CONS(__gcd(A, N) == 1);
    CONS(0 <= B && B < N);
	}

private:

};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"7 4 1"});
		Output({"843"});
	}

	void TestGroup1() {
    for (int i = 1 ; i <= 5 ; i++) {
      int n = rnd.nextInt(800, 1000);
      int a = getRandomCoprime(n);
      int b = rnd.nextInt(n);

      CASE(create_label(i),
           N = n, A = a, B = b);
    }
	}

private:
  int getRandomCoprime(int n) {
    vector<int> cand;
    for (int i = 1 ; i <= n ; i++) {
      if (__gcd(i, n) == 1) {
        cand.push_back(i);
      }
    }
    return cand[rnd.nextInt(cand.size())];
  }

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};