#include <bits/stdc++.h>
using namespace std;

struct {
  vector<int> solutions;

  void answer(int x) { solutions.push_back(x); }

  void print_solution() {
    long long M = 1000000007;
    long long idx = 1;
    long long res = 0;

    for (int sol : solutions) {
      long long val = (((idx * sol) % M) * sol) % M;
      res = (res + val) % M;
      idx++;
    }

    cout << res << '\n';
  }

} solutionEngine;

// Fungsi ini akan mengembalikan sekuens berukuran n+1, dengen seq[0] = -1 dan
// seq[i] = P_i untuk i > 0
vector<int> generate_sequence(int n, int a, int b) {
  vector<int> seq;
  seq.push_back(-1);

  for (int i = 1; i <= n; i++) {
    int val = (((1ll * a * i) % n) + b) % n;
    seq.push_back(val + 1);
  }

  return seq;
}

// Untuk memberikan jawaban, cukup panggil prosedur answer(x) yang mana x adalah
// monyet yang memakan pisang ke-i. Pastikan prosedur print_solution() dipanggil
// di bagian paling akhir dari main. Anda cukup menyalin hasil keluaran dari
// prosedur print_solution() kedalam template solusi yang diberikan.

int main() {
  // Lakukan proses input dan komputasi disini
  string label;
  int n, a, b;
  cin >> label;
  cin >> n >> a >> b;

  vector<int> sequence = generate_sequence(n, a, b);
  vector<int> banana(n+1, -1);

  for (int i = 0, active = n ; active > 0 ; i = (i + 1) % n) {
    int idx = i+1;
    int monkey = sequence[idx];

    for (int j = 1 ; j <= monkey ; j++)
      if (monkey % j == 0 && banana[j] == -1) {
        banana[j] = monkey;
        active--;
        break;
      }
  }

  for (int i = 1 ; i <= n ; i++) {
    solutionEngine.answer(banana[i]);
  }

  solutionEngine.print_solution();
  return 0;
}