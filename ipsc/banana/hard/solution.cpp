#include <bits/stdc++.h>
using namespace std;

struct {
  vector<int> solutions;

  void answer(int x) { solutions.push_back(x); }

  void print_solution() {
    long long M = 1000000007;
    long long idx = 1;
    long long res = 0;

    for (int sol : solutions) {
      long long val = (((idx * sol) % M) * sol) % M;
      res = (res + val) % M;
      idx++;
    }

    cout << res << '\n';
  }

} solutionEngine;

// Fungsi ini akan mengembalikan sekuens berukuran n+1, dengen seq[0] = -1 dan
// seq[i] = P_i untuk i > 0
vector<int> generate_sequence(int n, int a, int b) {
  vector<int> seq;
  seq.push_back(-1);

  for (int i = 1; i <= n; i++) {
    int val = (((1ll * a * i) % n) + b) % n;
    seq.push_back(val + 1);
  }

  return seq;
}

// Untuk memberikan jawaban, cukup panggil prosedur answer(x) yang mana x adalah
// monyet yang memakan pisang ke-i. Pastikan prosedur print_solution() dipanggil
// di bagian paling akhir dari main. Anda cukup menyalin hasil keluaran dari
// prosedur print_solution() kedalam template solusi yang diberikan.

int main() {
  // Lakukan proses input dan komputasi disini
  string label;
  int n, a, b;
  cin >> label;
  cin >> n >> a >> b;

  // outlier, sample
  if (label == "#0") {
    cout << 843 << endl;
    return 0;
  }

  assert(a == 1);
  assert(b == n-1);

  vector<int> v = {n, n, n+1, n+1};
  if (n % 2 == 0) {
    v[0] /= 2;
    v[1] /= 2;
  } else {
    v[2] /= 2;
    v[3] /= 2;
  }

  long long ret = 1;
  const int MOD = 1e9 + 7;
  for (int x : v) {
    ret = 1ll * ret * x % MOD;
  }

  cout << ret << endl;
  return 0;
}