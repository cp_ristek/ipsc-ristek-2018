import java.util.Scanner;

public class BananaHardSolutionTemplate {
    // Masukkan jawaban anda untuk setiap kasus uji didalam variabel dibawah ini,
    // Misalkan jawaban anda untuk kasus uji nomor 2 adalah 3, maka baris dibawah akan menjadi 'static int jawaban_2 = 3;' (tanpa tanda petik)
    static int jawaban_0 = 843;
    static int jawaban_1 = ;
    static int jawaban_2 = ;
    static int jawaban_3 = ;
    static int jawaban_4 = ;
    static int jawaban_5 = ;

    public static void main(String[] args) {
        String label = (new Scanner(System.in)).next();
        if ("#0".equals(label)) System.out.println(jawaban_0);
        if ("#1".equals(label)) System.out.println(jawaban_1);
        if ("#2".equals(label)) System.out.println(jawaban_2);
        if ("#3".equals(label)) System.out.println(jawaban_3);
        if ("#4".equals(label)) System.out.println(jawaban_4);
        if ("#5".equals(label)) System.out.println(jawaban_5);
    }
}