#include <bits/stdc++.h>
using namespace std;

const int N = 50005;

int arr[3][N];
int n;

void read() {
  string label; cin >> label;

  scanf("%d", &n);
  for (int i = 0 ; i < 3 ; i++)
    for (int j = 0 ; j < n ; j++) {
      scanf("%d", &arr[i][j]);
    }
}

int work() {
  int ret = 2e9;

  for (int i = 0 ; i < n ; i++)
    for (int j = 0 ; j < n ; j++)
      for (int k = 0 ; k < n ; k++) {
        int val = abs(arr[0][i] - arr[1][j]);
        val += abs(arr[0][i] - arr[2][k]);
        val += abs(arr[1][j] - arr[2][k]);

        ret = min(ret, val);
      }
  
  return ret;
}

int main() {
  read();
  printf("%d\n", work());
  return 0;
}