#include <bits/stdc++.h>
using namespace std;

const int N = 50005;

int arr[3][N];
int n;

void read() {
  string label; cin >> label;

  scanf("%d", &n);
  for (int i = 0 ; i < 3 ; i++)
    for (int j = 0 ; j < n ; j++) {
      scanf("%d", &arr[i][j]);
    }
}

int getMins(int a[], int b[], int c[]) {
  int ret = 2e9;
  for (int i = 0 ; i < n ; i++) {
    int val = a[i];

    int kiri = upper_bound(b, b + n, val) - b;
    kiri--;

    int kanan = lower_bound(c, c + n, val) - c;
    if (kiri >= 0 && kanan < n) {
      ret = min(ret, -2 * b[kiri] + 2 * c[kanan]);
    }
  }
  return ret;
}

int work() {
  for (int i = 0 ; i < 3 ; i++) {
    sort(arr[i], arr[i] + n);
  }
  int ret = 2e9;

  vector<int> perm = {0, 1, 2};
  do {
    ret = min(ret, getMins(arr[perm[0]], arr[perm[1]], arr[perm[2]]));
  } while (next_permutation(perm.begin(), perm.end()));
  
  return ret;
}

int main() {
  read();
  printf("%d\n", work());
  return 0;
}