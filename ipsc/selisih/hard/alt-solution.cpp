// sanity check using dp bitmask
#include <bits/stdc++.h>
using namespace std;

const int N = 50005;

int arr[3][N];
int n;

vector<pair<int, int>> v;
int dp[3 * N][8];
int coef[] = {-2, 0, 2};

void read() {
  string label; cin >> label;

  scanf("%d", &n);
  for (int i = 0 ; i < 3 ; i++)
    for (int j = 0 ; j < n ; j++) {
      scanf("%d", &arr[i][j]);
    }
}

void prepare() {
  memset(dp, -1, sizeof dp);
  for (int i = 0 ; i < 3 ; i++)
    for (int j = 0 ; j < n ; j++) {
      v.push_back({arr[i][j], i});
    }
  sort(v.begin(), v.end());
}

int solve(int now, int mask) {
  if (mask == 7) return 0;
  if (now == v.size()) return 1e9;

  int &ret = dp[now][mask];
  if (ret != -1) return ret;

  int cur = v[now].first;
  int bit = v[now].second;

  ret = solve(now+1, mask);
  if ((mask & (1 << bit)) == 0) {
    int add = coef[__builtin_popcount(mask)];
    ret = min(ret, add * cur + solve(now+1, mask | (1 << bit)));
  }

  return ret;
}

int work() {
  return solve(0, 0);
}

int main() {
  read();
  prepare();
  printf("%d\n", work());
  return 0;
}