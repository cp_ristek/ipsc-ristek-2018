#include <tcframe/runner.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N;
	vector<vector<int>> A;
  int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(N);
		GRID(A) % SIZE(3, N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 50000);
		CONS(eachElementBetween(A, 1, 100000000));
	}

private:
	bool eachElementBetween(vector<vector<int>> &v, int l, int r) {
		for (vector<int> row : v) {
      for (int x : row) {
        if (x < l || x > r) {
          return false;
        }
      }
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"4",
				"28 20 15 3",
				"11 14 8 9",
        "19 22 5 7"});
		Output({"10"});
	}

	void BeforeTestCase() {
		A.clear();
	}

	void TestGroup1() {
		for (int i=1;i<=2;++i) {
			CASE(create_label(i), N = rnd.nextInt(49000, 50000), A = randomGrid(3, N, 1, 1e8));
		}
    CASE(create_label(3), N = 50000, A = answerZeroGrid(3, N, 1, 1e8));
    for (int i=4;i<=5;++i) {
			CASE(create_label(i), N = rnd.nextInt(20000, 30000), A = randomGrid(3, N, 1, 1e8));
		}
	}

private:

	vector<vector<int>> randomGrid(int row, int column, int lo, int hi) {
    vector<vector<int>> ret;

    for (int i = 0 ; i < row ; i++) {
      vector<int> r(column, 0);
      for (int &x : r) {
        x = rnd.nextInt(lo, hi);
      }
      ret.push_back(r);
    }

    return ret;
  }

  vector<vector<int>> answerZeroGrid(int row, int column, int lo, int hi) {
    vector<vector<int>> ret = randomGrid(row, column, lo, hi);

    int same = rnd.nextInt(lo, hi);
    for (int i = 0 ; i < row ; i++) {
      int idx = rnd.nextInt(column);

      ret[i][idx] = same;
    }

    return ret;
  }

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}
};