class SolutionEngine():
	
	solutions = []

	def answer(self, x):
		self.solutions.append(x)

	def print_solution(self):
		M = 1000000007
		res = 0
		p = 13
		for sol in self.solutions:
			res = (res + (p*sol)%M)%M
			p = (p*13)%M
		print(res)
solutionEngine = SolutionEngine()
# Untuk memberikan jawaban, cukup panggil method solutionEngine.answer(x) dimana x adalah konstanta Chanekian setelah suatu perintah dilakukan.
# Pastikan method solutionEngine.print_solution() dipanggil di bagian paling akhir dari program ini.
# Anda cukup menyalin hasil keluaran dari method solutionEngine.print_solution() kedalam template solusi yang diberikan.
    

# Lakukan proses input dan komputasi disini

solutionEngine.print_solution()