#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
typedef long long ll;

struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		int M = 1000000007;
		long long res = 0;
		long long p = 13;
		for (int sol : solutions) {
			res = (res + (p*sol)%M)%M;
			p = (p*13)%M;
		}
		cout << res << '\n';
	}

} solutionEngine;
string label;

ll freq[1000005];
int num[100005];
int n,q;
int qidx, qval;

long long ans;

ll calc(ll a, ll b) {
	ll ret=1;
	while (b) {
		if (b&1) ret=(ret*a)%MOD;
		b>>=1;
		a=(a*a)%MOD;
	}
	return ret;
}

ll inv(ll x) {
	return calc(x,MOD-2);
}

int main() {
	cin >> label;
	cin >> n >> q;
	for (int i=1;i<=n;++i) {
		cin >> num[i];
		++freq[num[i]];
	}
	ans=1;
	for (int i=1;i<=1000000;++i) ans=(ans*(freq[i]+1))%MOD;
	for (int i=0;i<q;++i) {
		cin >> qidx >> qval;
		ans=(ans*inv(freq[num[qidx]]+1))%MOD;
		ans=(ans*inv(freq[qval]+1))%MOD;
		--freq[num[qidx]];
		++freq[qval];
		ans=(ans*(freq[num[qidx]]+1))%MOD;
		ans=(ans*(freq[qval]+1))%MOD;
		num[qidx] = qval;
		solutionEngine.answer(ans-1);
	}

	solutionEngine.print_solution();
	return 0;
}