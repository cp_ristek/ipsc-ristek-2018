#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string label;
	int N,Q;
	vector<int> A, Q_IDX, Q_VAL;
	int ANS;
	
	void InputFormat() {
		LINE(label);
		LINE(N,Q);
		LINE(A % SIZE(N));
		LINES(Q_IDX, Q_VAL) % SIZE(Q);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 100000);
		CONS(1 <= Q && Q <= 100000);
		CONS(eachElementBetween(A, 1, 1000000));
		CONS(eachElementBetween(Q_VAL, 1, 1000000));
		CONS(eachElementBetween(Q_IDX, 1, N));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"15 5",
				"9 10 9 10 8 10 8 9 9 7 8 7 10 6 10",
				"5 8",
				"12 529015",
				"8 392874",
				"14 71053",
				"6 9",
				});
		Output({"641082455"});
	}

	void BeforeTestCase() {
		A.clear();
		Q_IDX.clear();
		Q_VAL.clear();
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), N = rnd.nextInt(80000, 100000), Q = rnd.nextInt(80000, 100000), generate(N,Q));
		}
	}

private:

	void generate(int n, int q) {
		int minimum=rnd.nextInt(1,900000);
		int maximum=rnd.nextInt(minimum, minimum+(n/2));
		generateA(n, minimum, maximum);
		generateQWithFree(n, q, minimum, maximum);
	}

	void generateA(int n, int minimum, int maximum) {
		while (A.size() < n)
			A.push_back(rnd.nextInt(minimum, maximum));
	}

	void generateQWithFree(int n, int q, int minimum, int maximum) {
		while (Q_IDX.size() < q) {
			Q_IDX.push_back(rnd.nextInt(1,n));
			if (rnd.nextInt(0,5) <= 2) {
				Q_VAL.push_back(rnd.nextInt(minimum,maximum));
			} else {
				Q_VAL.push_back(rnd.nextInt(1,1000000));
			}
		}
	}

	void create_label(int i) {
		ostringstream ss;
		ss << i;
		label = "#" + ss.str();
	}

};