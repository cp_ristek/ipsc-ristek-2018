#include <bits/stdc++.h>
using namespace std;


struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		int M = 1000000007;
		long long res = 0;
		long long p = 13;
		for (int sol : solutions){
			res = (res + (p*sol)%M)%M;
			p = (p*13)%M;
		}
		cout << res << '\n';
	}

} solutionEngine;

// Untuk memberikan jawaban, cukup panggil prosedur answer(x) dimana x adalah konstanta Chanekian setelah suatu perintah dilakukan.
// Pastikan prosedur print_solution() dipanggil di bagian paling akhir dari main.
// Anda cukup menyalin hasil keluaran dari prosedur print_solution() kedalam template solusi yang diberikan.

int main() {
	// Lakukan proses input dan komputasi disini


	solutionEngine.print_solution();
	return 0;
}