#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;

struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		int M = 1000000007;
		long long res = 0;
		long long p = 13;
		for (int sol : solutions){
			res = (res + (p*sol)%M)%M;
			p = (p*13)%M;
		}
		cout << res << '\n';
	}

} solutionEngine;

int freq[1000005];
int num[25];
int n,q;
int qidx, qval;

long long ans;
string label;

int main() {
	cin >> label;
	cin >> n >> q;
	for (int i=1;i<=n;++i) {
		cin >> num[i];
		++freq[num[i]];
	}

	cin >> qidx >> qval;
	--freq[num[qidx]];
	num[qidx] = qval;
	++freq[qval];

	ans=1;

	for (int i=1;i<=1000000;++i) {
		ans=(ans*(freq[i]+1))%MOD;
	}

	solutionEngine.answer(ans-1);

	solutionEngine.print_solution();
	return 0;
}