import java.util.List;
import java.util.ArrayList;

public class KonstantaChanekianSolutionHelper {

    static class SolutionEngine{
        private static List<Integer> solutions = new ArrayList<>();

        static void answer(int x) {
            solutions.add(x);
        }

        static void print_solution() {
            int M = 1000000007;
            long res = 0;
            long p = 13;
            for (int sol : solutions) {
                res = (res + (p*sol)%M)%M;
                p = (p*13L)%M;
            }
            System.out.println(res);
        }
    }


    // Untuk memberikan jawaban, cukup panggil method answer(x) dimana x adalah konstanta Chanekian setelah suatu perintah dilakukan.
    // Pastikan method print_solution() dipanggil di bagian paling akhir dari main.
    // Anda cukup menyalin hasil keluaran dari method print_solution() kedalam template solusi yang diberikan.
    
    public static void main(String[] args) {
        // Lakukan proses input dan komputasi disini

        SolutionEngine.print_solution();
    }
}
